export default function ($location, AuthService, FeedbackService)
{	
	'ngInject';
		
	var vm = this;

	vm.loading = false;

	vm.logIn = (username, password) =>
	{	
		vm.loading = true;

		AuthService.logIn(username, password)
		
			.then(() =>
			{
				$location.path('/');
				
			}).catch(() =>
			{	
				FeedbackService.showMessage('negative', 'Login credentials incorrect, please try again.');

			}).finally(() =>
			{
				vm.loading = false;
			});
	}
}
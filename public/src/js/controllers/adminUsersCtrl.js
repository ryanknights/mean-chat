export default function (currentUser, users, Admin, FeedbackService, Chat)
{	
	'ngInject';
		
	var vm = this;

	vm.users = users;

	/*----------  Delete User  ----------*/
	
	vm.deleteUser = function (index, userID)
	{
		Admin.deleteUser(userID).then(() =>
		{	
			FeedbackService.showMessage('positive', 'User deleted');

			vm.users.splice(index, 1);

		}).catch((err) =>
		{	
			FeedbackService.showMessage('negative', err.data);			
		});
	}

	Chat.connect(currentUser);
}
export default function (currentUser, profile, Chat, ProfileService, FeedbackService)
{	
	'ngInject';
		
	var vm = this;

	vm.data = profile;

	vm.updateProfile = (data) =>
	{
		ProfileService.updateProfile(data).then((result) =>
		{
			if (result.data.success)
			{	
				FeedbackService.showMessage('positive', 'Your profile is updated');
			}

		}).catch((err) =>
		{
			FeedbackService.showMessage('negative', err.data);
		});
	}

	Chat.connect(currentUser);
}
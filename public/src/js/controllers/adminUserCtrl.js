export default function (currentUser, user, Admin, FeedbackService, Chat)
{	
	'ngInject';
		
	var vm = this;

	vm.user  = user;	

	vm.editUser = (userID, userData) =>
	{
		Admin.updateUser(userID, userData).then((result) =>
		{
			if (result.data.success)
			{
				FeedbackService.showMessage('positive', 'User is updated');
			}

		}).catch((err) =>
		{	
			FeedbackService.showMessage('negative', err.data);
		});
	}

	vm.updatePassword = (userID, newPassword) =>
	{
		Admin.updatePassword(userID, newPassword).then((result) =>
		{
			if (result.data.success)
			{
				FeedbackService.showMessage('positive', 'Password is updated');	
			}

		}).catch((err) =>
		{	
			FeedbackService.showMessage('negative', err.data);
		});
	}

	Chat.connect(currentUser);
}
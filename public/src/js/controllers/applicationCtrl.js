export default function ($scope, $rootScope, $location, AuthService, CurrentUserService, Socket)
{	
	'ngInject';
	
	const vm = this;

	vm.currentUser = null;
	vm.isLoggedIn  = false;

	$scope.$on('auth-login-success', (event) =>
	{	
		console.log('Event => auth-login-success');
		vm.isLoggedIn  = true;
		vm.currentUser = CurrentUserService.getUser();
	});

	$scope.$on('auth-login-reauthenticate-success', (event) =>
	{	
		console.log('Event => auth-login-reauthenticate-success');
		vm.isLoggedIn  = true;
		vm.currentUser = CurrentUserService.getUser();
	});	

	$scope.$on('auth-login-failed', (event) =>
	{	
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		console.log('Event => auth-login-failed');
	});

	$scope.$on('auth-logout-success', (event) =>
	{
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		console.log('Event => auth-logout-success');
	});

	$scope.$on('auth-not-authenticated', (event) =>
	{
		vm.currentUser = null;
		vm.isLoggedIn  = false;
		console.log('Event => auth-not-authenticated');
	});

	$scope.$on('auth-not-authorized', (event) =>
	{
		console.log('Event => auth-not-authorized');
	});

	vm.logOut = function ()
	{
		AuthService.logOut();
		Socket.emit('leaveChat');
		$location.path('/login');	
	}
}
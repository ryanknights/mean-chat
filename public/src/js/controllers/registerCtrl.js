export default function ($location, AccountService, AuthService, FeedbackService)
{	
	'ngInject';
		
	var vm = this;

	vm.register = (username, email, password) =>
	{
		AccountService.register(username, email, password).then(() =>
		{
			AuthService.logIn(username, password)

				.then((result) =>
				{	
					$location.path('/');
					
				}).catch((err) =>
				{	
					FeedbackService.showMessage('warning', 'There was a problem logging you in.');
				});

		}).catch((err) =>
		{
			let errState = '';

			switch (err.status)
			{
				case 500: 
				{	
					errState = 'negative';
					break;
				}

				case 400:
				{
					errState = 'warning';
					break;
				}

				default:
				{
					errState = 'negative';
				}
			}			

			FeedbackService.showMessage(errState, err.data);
		});			
	}
}
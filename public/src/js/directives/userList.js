export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-userList.html',
		scope            : {},
		controller (Socket, FeedbackService, Chat, CurrentUserService)
		{	
			var vm = this;

			vm.chat        = Chat.data;
			vm.currentUser = CurrentUserService.getUser();

			vm.requestPrivateChat = (userID) =>
			{
				Socket.emit('requestPrivateChat', userID, (err) =>
				{
					if (err)
					{
						return FeedbackService.showMessage('negative', err);
					}
				});
			}			
		},
		controllerAs : 'vm'
	}
}
export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-chatPane.html',
		scope            : {},
		controller (Socket, Chat)
		{	
			var vm = this;

			vm.messages   = Chat.data.messages;
			vm.newMessage = '';

			vm.sendMessage = (message) =>
			{
				Socket.emit('sendMessage', message);
				vm.newMessage = '';
			}					
		},
		controllerAs : 'vm'
	}
}
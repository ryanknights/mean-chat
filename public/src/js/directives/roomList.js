export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-roomList.html',
		scope            : {},
		controller (Socket, $window, FeedbackService, Chat, CurrentUserService)
		{	
			var vm = this;

			vm.chat        = Chat.data;
			vm.currentUser = CurrentUserService.getUser();	

			vm.newRoomName     = '';
			vm.newRoomMaxUsers = '';

			/*----------  Switch room  ----------*/

			vm.switchRoom = (room) =>
			{
				Socket.emit('switchRoom', room, false, (err) =>
				{
					if (err)
					{
						return FeedbackService.showMessage('negative', err);
					}			

					FeedbackService.resetMessage();
				});
			}

			/*----------  Add new room  ----------*/

			vm.addRoom = (newRoomName, maxUsers) =>
			{
				Socket.emit('createRoom', newRoomName, maxUsers, (err) =>
				{
					if (err)
					{
						return FeedbackService.showMessage('negative', err);
					}

					vm.newRoomName     = '';
					vm.newRoomMaxUsers = '';

					FeedbackService.resetMessage();
				});
			}

			/*----------  Delete room  ----------*/

			vm.deleteRoom = (roomToDelete) =>
			{
				Socket.emit('deleteRoom', roomToDelete, $window.sessionStorage.token, (err) =>
				{
					if (err)
					{
						return FeedbackService.showMessage('negative', err);
					}

					FeedbackService.resetMessage();
				});
			}	
		},
		controllerAs : 'vm'
	}
}
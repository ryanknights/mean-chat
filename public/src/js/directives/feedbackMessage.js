export default function ()
{	
	'ngInject';
		
	return {

		restrict         : 'AE',
		replace          : true,
		templateUrl      : '/src/js/views/directive-feedbackMessage.html',
		scope            : {},	
		controller (FeedbackService)
		{	
			var vm = this;

			vm.message = FeedbackService.message;

			vm.closeMessage = () => FeedbackService.resetMessage();
		},
		controllerAs : 'vm'
	}
}
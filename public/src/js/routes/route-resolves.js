/*----------  Resolve Current User  ----------*/

export function currentUser ($q, $location, AuthService, CurrentUserService)
{
	if (AuthService.isLoggedIn())
	{
		return CurrentUserService.getUser();
	}
	else if (AuthService.userIsReauthenticating)
	{
		return AuthService.reAuthenticate().then(() =>
		{
			return CurrentUserService.getUser();

		}).catch((res) =>
		{	
			return $q.reject(res);
		});
	}
	else
	{
		$location.path('/login');
		return $q.reject();
	}	
}

/*----------  Resolve Profile  ----------*/

export function profile ($q, ProfileService)
{
	return ProfileService.retrieveProfile().then((res) =>
	{
		return res.data.profile;

	}, (res) =>
	{
		return $q.reject(res);
	});
}

/*----------  Resolve Admin Users  ----------*/

export function users (Admin)
{
	return Admin.retrieveUsers().then((res) =>
	{
		return res.data.users;

	}, (res) =>
	{
		return $q.reject(res);
	});
}

/*----------  Resolve Admin User  ----------*/

export function user ($q, $route, Admin)
{
	return Admin.retrieveUser($route.current.params.id).then((res) =>
	{
		return res.data.user;

	}, (res) =>
	{
		return $q.reject(res);
	});	
}



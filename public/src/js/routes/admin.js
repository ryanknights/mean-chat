export default function ($routeProvider, $locationProvider)
{	
	'ngInject';

	import {currentUser, users, user} from './route-resolves';
		
	$routeProvider
	
		.when('/admin/users',
		{
			templateUrl  : 'src/js/views/admin-users.html',
			controller   : 'adminUsersCtrl',
			controllerAs : 'adminUsers',
			access       : { requiredLogin: true, isAdmin: true},
			resolve      :
			{	
				currentUser : currentUser,
				users       : users
			}
		})
		.when('/admin/users/:id',
		{
			templateUrl  : 'src/js/views/admin-user.html',
			controller   : 'adminUserCtrl',
			controllerAs : 'adminUser',
			access       : { requiredLogin: true, isAdmin: true},
			resolve      :
			{	
				currentUser : currentUser,
				user        : user
			}	
		});
}
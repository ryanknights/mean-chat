export default function ($routeProvider, $locationProvider)
{	
	'ngInject';

	import {currentUser, profile} from './route-resolves';
		
	$routeProvider

		.when('/',
		{	
			templateUrl  : '/src/js/views/chat.html',
			controller   : 'chatCtrl',
			controllerAs : 'chat',
			resolve      :
			{
				currentUser : currentUser
			}
		})
		.when('/profile',
		{
			templateUrl  : '/src/js/views/profile.html',
			controller   : 'profileCtrl',
			controllerAs : 'profile',
			access       : { requiredLogin: true }, 
			resolve      :
			{	
				currentUser : currentUser,
				profile     : profile
			}			
		})
		.when('/login',
		{	
			templateUrl  : '/src/js/views/login.html',
			controller   : 'loginCtrl',
			controllerAs : 'login',
			access       : { requiredLogin: false } 
		})
		.when('/register',
		{	
			templateUrl  : '/src/js/views/register.html',
			controller   : 'registerCtrl',
			controllerAs : 'register',
			access       : { requiredLogin: false }
		})
}
export default function ($window)
{	
	'ngInject';
		
	const CurrentUserService = {};

	CurrentUserService.user = null;

	CurrentUserService.setUser = (user, token) =>
	{	
		if (user === undefined)
		{
			throw new Error('setUser requires a valid user object');
			return false;
		}

		CurrentUserService.user = {};

		CurrentUserService.user.userid   = user.userid;
		CurrentUserService.user.username = user.username;
		CurrentUserService.user.isAdmin  = user.isAdmin;

		if (token !== undefined)
		{
			$window.sessionStorage.token = token;
		}
	}

	CurrentUserService.getUser = () =>
	{
		return (CurrentUserService.user !== null)? CurrentUserService.user : false;
	}

	CurrentUserService.destroy = () =>
	{
		CurrentUserService.user = null;

		delete $window.sessionStorage.token;
	}

	return CurrentUserService;
}


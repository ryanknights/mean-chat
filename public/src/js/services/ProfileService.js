export default function ($http)
{	
	'ngInject';
		
	const Profile = {};

	Profile.retrieveProfile = () => $http.get('/api/profile/');
	Profile.updateProfile = (data) => $http.put('/api/profile', data);

	return Profile;
}


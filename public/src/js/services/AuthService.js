export default function ($rootScope, $http, CurrentUserService, Chat, $q)
{	
	'ngInject';
		
	const authService = {};

	authService.userIsReauthenticating = false;

	authService.isLoggedIn = () =>
	{
		return (CurrentUserService.user !== null);
	}

	authService.isAdmin = () =>
	{	
		if (!CurrentUserService.user)
		{
			return false;
		}
		
		return (CurrentUserService.user.isAdmin === true);
	}

	authService.logIn = (username, password) =>
	{	
		let deferred = $q.defer();

		$http.post('/api/login', {username : username, password : password})

			.then((res) =>
			{
				CurrentUserService.setUser(res.data.user, res.data.token);
				
				$rootScope.$broadcast('auth-login-success');

				deferred.resolve();

			}, () =>
			{
				$rootScope.$broadcast('auth-login-failed');

				deferred.reject();
			});

		return deferred.promise;
	}

	authService.logOut = () =>
	{
		authService.userIsReauthenticating = false;
		Chat.isConnected = false;

		CurrentUserService.destroy();

		$rootScope.$broadcast('auth-logout-success');
	}	

	authService.reAuthenticate = () =>
	{
		return $http.get('/api/authenticate')

				.then((res) =>
				{
					CurrentUserService.setUser(res.data.user);

					authService.userIsReauthenticating = false;

					$rootScope.$broadcast('auth-login-reauthenticate-success');

					return res.data.user;

				}, () =>
				{	
					$rootScope.$broadcast('auth-login-reauthenticate-failed');

					return null;
				});
	}

	return authService;
}


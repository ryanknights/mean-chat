export default function (Socket, CurrentUserService)
{	
	'ngInject';
		
	const Chat = {};

	Chat.isConnected = false;
	
	Chat.data =
	{
		activeUsers     : [],
		messages        : [],
		rooms           : {},
		roomCounts      : {}
	};

	Chat.connect = (currentUser, callback) =>
	{	
		if (!Chat.isConnected)
		{
			Socket.emit('join', currentUser, () =>
			{	
				Chat.isConnected = true;

				if (angular.isFunction(callback))
				{
					callback();
				}
			});
		}
		else // If chat is already connected and they have returned to this room, i.e. admin/profile pages just reassign the chat data
		{
			if (angular.isFunction(callback))
			{
				callback();
			}
		}
	}

	Socket.on('recieveUsers', (users) =>
	{
		Chat.data.activeUsers = users;
	});

	Socket.on('recieveRoomCounts', (roomCounts) =>
	{
		Chat.data.roomCounts = roomCounts;
	});	

	Socket.on('message', (messageObj) =>
	{
		Chat.data.messages.push(messageObj);
	});

	Socket.on('recieveRooms', (rooms) =>
	{
		Chat.data.rooms = rooms;
	});

	Socket.on('recieveCurrentRoom', (currentRoom) =>
	{
		Chat.data.currentRoom = currentRoom;
	});

	Socket.on('roomDeleted', (roomToJoin) =>
	{
		Socket.emit('switchRoom', roomToJoin, true);
	});

	Socket.on('recievePrivateChat', (user) =>
	{	
		var acceptPrivateChat = confirm(`${user.username} has requested a private chat, would you like to accept?`);

		Socket.emit('sendPrivateChatResponse', acceptPrivateChat, user);
	});

	Socket.on('initPrivateChat', (roomToJoin, rooms) =>
	{
		Socket.emit('switchRoom', roomToJoin, false);
		Chat.data.rooms = rooms;
	});	

	return Chat;
}


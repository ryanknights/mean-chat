export default function ($http)
{	
	'ngInject';
		
	const Admin = {};

	Admin.retrieveUsers = () => $http.get('/api/admin/users');
	Admin.deleteUser = (userID) => $http.delete('/api/admin/users/' + userID);
	Admin.retrieveUser = (userID) => $http.get('/api/admin/users/' + userID);
	Admin.updateUser = (userID, data) => $http.put('/api/admin/users/' + userID, data);
	Admin.updatePassword = (userID, newPassword) => $http.put('/api/admin/users/' + userID + '/password', {newPassword: newPassword});

	return Admin;
}


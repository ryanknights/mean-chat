export default function ($http)
{	
	'ngInject';
		
	const accountService = {};

	accountService.register = (username, email, password) => $http.post('/api/register', {username : username, email : email, password : password});

	return accountService;
}
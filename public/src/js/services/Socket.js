export default function (socketFactory)
{	
	'ngInject';
		
	return socketFactory();
}
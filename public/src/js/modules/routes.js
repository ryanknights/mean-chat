import mainRoutes from '../routes/main';
import adminRoutes from '../routes/admin';

const module = angular.module('mean-chat.routes', [])
	.config(mainRoutes)
	.config(adminRoutes);

export default module;
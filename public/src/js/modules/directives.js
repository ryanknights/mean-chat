import userList from '../directives/userList';
import roomList from '../directives/roomList';
import chatPane from '../directives/chatPane';
import feedbackMessage from '../directives/feedbackMessage';

const module = angular.module('mean-chat.directives', [])
	.directive('userList', userList)
	.directive('roomList', roomList)
	.directive('chatPane', chatPane)
	.directive('feedbackMessage', feedbackMessage);

export default module;
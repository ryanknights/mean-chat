import includeTokenInterceptor from '../config/includeTokenInterceptor';
import security from '../config/security';

const module = angular.module('mean-chat.config', [])
	.config(includeTokenInterceptor)
	.run(security);

export default module;
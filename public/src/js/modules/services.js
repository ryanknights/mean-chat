import AccountService from '../services/AccountService';
import AuthService from '../services/AuthService';
import CurrentUserService from '../services/CurrentUserService';
import SocketService from '../services/Socket';
import TokenInterceptor from '../services/TokenInterceptor';
import Chat from '../services/Chat';
import Admin from '../services/Admin';
import ProfileService from '../services/ProfileService';
import FeedbackService from '../services/Feedback';

const module = angular.module('mean-chat.services', [])
	.factory('AccountService', AccountService)
	.factory('AuthService', AuthService)
	.factory('CurrentUserService', CurrentUserService)
	.factory('Socket', SocketService)
	.factory('TokenInterceptor', TokenInterceptor)
	.factory('Chat', Chat)
	.factory('Admin', Admin)
	.factory('ProfileService', ProfileService)
	.factory('FeedbackService', FeedbackService);

export default module;
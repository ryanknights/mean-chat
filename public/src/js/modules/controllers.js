import applicationCtrl from '../controllers/applicationCtrl';
import chatCtrl from '../controllers/chatCtrl';
import loginCtrl from '../controllers/loginCtrl';
import registerCtrl from '../controllers/registerCtrl';
import adminUsersCtrl from '../controllers/adminUsersCtrl';
import adminUserCtrl from '../controllers/adminUserCtrl';
import profileCtrl from '../controllers/profileCtrl';


const module = angular.module('mean-chat.controllers', [])
	.controller('applicationCtrl', applicationCtrl)
	.controller('chatCtrl', chatCtrl)
	.controller('loginCtrl', loginCtrl)
	.controller('registerCtrl', registerCtrl)
	.controller('adminUsersCtrl', adminUsersCtrl)
	.controller('adminUserCtrl', adminUserCtrl)
	.controller('profileCtrl', profileCtrl);

export default module;
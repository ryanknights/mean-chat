import Config from './modules/config';
import Controllers from './modules/controllers';
import Routes from './modules/routes';
import Services from './modules/services';
import Directives from './modules/directives';

angular.module('mean-chat', ['ngRoute', 'btford.socket-io', Config.name, Controllers.name, Routes.name, Services.name, Directives.name]);
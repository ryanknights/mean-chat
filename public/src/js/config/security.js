export default ($rootScope, $location, $window, AuthService) =>
{	
	'ngInject';

	function preventNotAuthenticated (event)
	{
    	event.preventDefault();

		$rootScope.$broadcast('auth-not-authenticated');  

		$location.path('/login');
	}

	function preventNotAuthorized (event)
	{
    	$rootScope.$broadcast('auth-not-authorized');

    	$location.path('/');		
	}
	
	if (window.sessionStorage.token) // If we have a token stored we assume a user has been logged in
	{	
		AuthService.userIsReauthenticating = true;
	}

	$rootScope.$on('$routeChangeStart', (event, nextRoute, currentRoute) =>
	{
	    if (nextRoute.access && nextRoute.access.requiredLogin && !nextRoute.access.isAdmin && !AuthService.isLoggedIn()) 
	    {
	    	if (AuthService.userIsReauthenticating)
	    	{
	    		AuthService.reAuthenticate().then((user) =>
    			{
    				if (!user)
    				{
    					return preventNotAuthenticated(event);
    				}
    			});
	    	}
	    	else
	    	{
	    		return preventNotAuthenticated(event);
	    	}
	    }
	    else if (nextRoute.access && nextRoute.access.isAdmin && !AuthService.isAdmin())
	    {
			if (AuthService.userIsReauthenticating)
			{
				AuthService.reAuthenticate().then((user) =>
				{
					if (!user || (user && !user.isAdmin))
					{
						return preventNotAuthorized(event);
					}
				});
			}
			else
			{
				return preventNotAuthorized(event);
			}
	    }
	});
}
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _modulesConfig = require('./modules/config');

var _modulesConfig2 = _interopRequireDefault(_modulesConfig);

var _modulesControllers = require('./modules/controllers');

var _modulesControllers2 = _interopRequireDefault(_modulesControllers);

var _modulesRoutes = require('./modules/routes');

var _modulesRoutes2 = _interopRequireDefault(_modulesRoutes);

var _modulesServices = require('./modules/services');

var _modulesServices2 = _interopRequireDefault(_modulesServices);

var _modulesDirectives = require('./modules/directives');

var _modulesDirectives2 = _interopRequireDefault(_modulesDirectives);

angular.module('mean-chat', ['ngRoute', 'btford.socket-io', _modulesConfig2['default'].name, _modulesControllers2['default'].name, _modulesRoutes2['default'].name, _modulesServices2['default'].name, _modulesDirectives2['default'].name]);

},{"./modules/config":15,"./modules/controllers":16,"./modules/directives":17,"./modules/routes":18,"./modules/services":19}],2:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$httpProvider', function ($httpProvider) {
	'ngInject';

	$httpProvider.interceptors.push('TokenInterceptor');
}];

module.exports = exports['default'];

},{}],3:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', '$location', '$window', 'AuthService', function ($rootScope, $location, $window, AuthService) {
	'ngInject';

	function preventNotAuthenticated(event) {
		event.preventDefault();

		$rootScope.$broadcast('auth-not-authenticated');

		$location.path('/login');
	}

	function preventNotAuthorized(event) {
		$rootScope.$broadcast('auth-not-authorized');

		$location.path('/');
	}

	if (window.sessionStorage.token) // If we have a token stored we assume a user has been logged in
		{
			AuthService.userIsReauthenticating = true;
		}

	$rootScope.$on('$routeChangeStart', function (event, nextRoute, currentRoute) {
		if (nextRoute.access && nextRoute.access.requiredLogin && !nextRoute.access.isAdmin && !AuthService.isLoggedIn()) {
			if (AuthService.userIsReauthenticating) {
				AuthService.reAuthenticate().then(function (user) {
					if (!user) {
						return preventNotAuthenticated(event);
					}
				});
			} else {
				return preventNotAuthenticated(event);
			}
		} else if (nextRoute.access && nextRoute.access.isAdmin && !AuthService.isAdmin()) {
			if (AuthService.userIsReauthenticating) {
				AuthService.reAuthenticate().then(function (user) {
					if (!user || user && !user.isAdmin) {
						return preventNotAuthorized(event);
					}
				});
			} else {
				return preventNotAuthorized(event);
			}
		}
	});
}];

module.exports = exports['default'];

},{}],4:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['currentUser', 'user', 'Admin', 'FeedbackService', 'Chat', function (currentUser, user, Admin, FeedbackService, Chat) {
	'ngInject';

	var vm = this;

	vm.user = user;

	vm.editUser = function (userID, userData) {
		Admin.updateUser(userID, userData).then(function (result) {
			if (result.data.success) {
				FeedbackService.showMessage('positive', 'User is updated');
			}
		})['catch'](function (err) {
			FeedbackService.showMessage('negative', err.data);
		});
	};

	vm.updatePassword = function (userID, newPassword) {
		Admin.updatePassword(userID, newPassword).then(function (result) {
			if (result.data.success) {
				FeedbackService.showMessage('positive', 'Password is updated');
			}
		})['catch'](function (err) {
			FeedbackService.showMessage('negative', err.data);
		});
	};

	Chat.connect(currentUser);
}];

module.exports = exports['default'];

},{}],5:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['currentUser', 'users', 'Admin', 'FeedbackService', 'Chat', function (currentUser, users, Admin, FeedbackService, Chat) {
	'ngInject';

	var vm = this;

	vm.users = users;

	/*----------  Delete User  ----------*/

	vm.deleteUser = function (index, userID) {
		Admin.deleteUser(userID).then(function () {
			FeedbackService.showMessage('positive', 'User deleted');

			vm.users.splice(index, 1);
		})['catch'](function (err) {
			FeedbackService.showMessage('negative', err.data);
		});
	};

	Chat.connect(currentUser);
}];

module.exports = exports['default'];

},{}],6:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$scope', '$rootScope', '$location', 'AuthService', 'CurrentUserService', 'Socket', function ($scope, $rootScope, $location, AuthService, CurrentUserService, Socket) {
	'ngInject';

	var vm = this;

	vm.currentUser = null;
	vm.isLoggedIn = false;

	$scope.$on('auth-login-success', function (event) {
		console.log('Event => auth-login-success');
		vm.isLoggedIn = true;
		vm.currentUser = CurrentUserService.getUser();
	});

	$scope.$on('auth-login-reauthenticate-success', function (event) {
		console.log('Event => auth-login-reauthenticate-success');
		vm.isLoggedIn = true;
		vm.currentUser = CurrentUserService.getUser();
	});

	$scope.$on('auth-login-failed', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		console.log('Event => auth-login-failed');
	});

	$scope.$on('auth-logout-success', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		console.log('Event => auth-logout-success');
	});

	$scope.$on('auth-not-authenticated', function (event) {
		vm.currentUser = null;
		vm.isLoggedIn = false;
		console.log('Event => auth-not-authenticated');
	});

	$scope.$on('auth-not-authorized', function (event) {
		console.log('Event => auth-not-authorized');
	});

	vm.logOut = function () {
		AuthService.logOut();
		Socket.emit('leaveChat');
		$location.path('/login');
	};
}];

module.exports = exports['default'];

},{}],7:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['currentUser', 'Socket', 'Chat', function (currentUser, Socket, Chat) {
	'ngInject';

	var vm = this;

	Chat.connect(currentUser);
}];

module.exports = exports['default'];

},{}],8:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$location', 'AuthService', 'FeedbackService', function ($location, AuthService, FeedbackService) {
	'ngInject';

	var vm = this;

	vm.loading = false;

	vm.logIn = function (username, password) {
		vm.loading = true;

		AuthService.logIn(username, password).then(function () {
			$location.path('/');
		})['catch'](function () {
			FeedbackService.showMessage('negative', 'Login credentials incorrect, please try again.');
		})['finally'](function () {
			vm.loading = false;
		});
	};
}];

module.exports = exports['default'];

},{}],9:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['currentUser', 'profile', 'Chat', 'ProfileService', 'FeedbackService', function (currentUser, profile, Chat, ProfileService, FeedbackService) {
	'ngInject';

	var vm = this;

	vm.data = profile;

	vm.updateProfile = function (data) {
		ProfileService.updateProfile(data).then(function (result) {
			if (result.data.success) {
				FeedbackService.showMessage('positive', 'Your profile is updated');
			}
		})['catch'](function (err) {
			FeedbackService.showMessage('negative', err.data);
		});
	};

	Chat.connect(currentUser);
}];

module.exports = exports['default'];

},{}],10:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$location', 'AccountService', 'AuthService', 'FeedbackService', function ($location, AccountService, AuthService, FeedbackService) {
	'ngInject';

	var vm = this;

	vm.register = function (username, email, password) {
		AccountService.register(username, email, password).then(function () {
			AuthService.logIn(username, password).then(function (result) {
				$location.path('/');
			})['catch'](function (err) {
				FeedbackService.showMessage('warning', 'There was a problem logging you in.');
			});
		})['catch'](function (err) {
			var errState = '';

			switch (err.status) {
				case 500:
					{
						errState = 'negative';
						break;
					}

				case 400:
					{
						errState = 'warning';
						break;
					}

				default:
					{
						errState = 'negative';
					}
			}

			FeedbackService.showMessage(errState, err.data);
		});
	};
}];

module.exports = exports['default'];

},{}],11:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-chatPane.html',
		scope: {},
		controller: function controller(Socket, Chat) {
			var vm = this;

			vm.messages = Chat.data.messages;
			vm.newMessage = '';

			vm.sendMessage = function (message) {
				Socket.emit('sendMessage', message);
				vm.newMessage = '';
			};
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],12:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-feedbackMessage.html',
		scope: {},
		controller: function controller(FeedbackService) {
			var vm = this;

			vm.message = FeedbackService.message;

			vm.closeMessage = function () {
				return FeedbackService.resetMessage();
			};
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],13:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-roomList.html',
		scope: {},
		controller: function controller(Socket, $window, FeedbackService, Chat, CurrentUserService) {
			var vm = this;

			vm.chat = Chat.data;
			vm.currentUser = CurrentUserService.getUser();

			vm.newRoomName = '';
			vm.newRoomMaxUsers = '';

			/*----------  Switch room  ----------*/

			vm.switchRoom = function (room) {
				Socket.emit('switchRoom', room, false, function (err) {
					if (err) {
						return FeedbackService.showMessage('negative', err);
					}

					FeedbackService.resetMessage();
				});
			};

			/*----------  Add new room  ----------*/

			vm.addRoom = function (newRoomName, maxUsers) {
				Socket.emit('createRoom', newRoomName, maxUsers, function (err) {
					if (err) {
						return FeedbackService.showMessage('negative', err);
					}

					vm.newRoomName = '';
					vm.newRoomMaxUsers = '';

					FeedbackService.resetMessage();
				});
			};

			/*----------  Delete room  ----------*/

			vm.deleteRoom = function (roomToDelete) {
				Socket.emit('deleteRoom', roomToDelete, $window.sessionStorage.token, function (err) {
					if (err) {
						return FeedbackService.showMessage('negative', err);
					}

					FeedbackService.resetMessage();
				});
			};
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],14:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function () {
	'ngInject';

	return {

		restrict: 'AE',
		replace: true,
		templateUrl: '/src/js/views/directive-userList.html',
		scope: {},
		controller: function controller(Socket, FeedbackService, Chat, CurrentUserService) {
			var vm = this;

			vm.chat = Chat.data;
			vm.currentUser = CurrentUserService.getUser();

			vm.requestPrivateChat = function (userID) {
				Socket.emit('requestPrivateChat', userID, function (err) {
					if (err) {
						return FeedbackService.showMessage('negative', err);
					}
				});
			};
		},
		controllerAs: 'vm'
	};
};

module.exports = exports['default'];

},{}],15:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _configIncludeTokenInterceptor = require('../config/includeTokenInterceptor');

var _configIncludeTokenInterceptor2 = _interopRequireDefault(_configIncludeTokenInterceptor);

var _configSecurity = require('../config/security');

var _configSecurity2 = _interopRequireDefault(_configSecurity);

var _module = angular.module('mean-chat.config', []).config(_configIncludeTokenInterceptor2['default']).run(_configSecurity2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../config/includeTokenInterceptor":2,"../config/security":3}],16:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _controllersApplicationCtrl = require('../controllers/applicationCtrl');

var _controllersApplicationCtrl2 = _interopRequireDefault(_controllersApplicationCtrl);

var _controllersChatCtrl = require('../controllers/chatCtrl');

var _controllersChatCtrl2 = _interopRequireDefault(_controllersChatCtrl);

var _controllersLoginCtrl = require('../controllers/loginCtrl');

var _controllersLoginCtrl2 = _interopRequireDefault(_controllersLoginCtrl);

var _controllersRegisterCtrl = require('../controllers/registerCtrl');

var _controllersRegisterCtrl2 = _interopRequireDefault(_controllersRegisterCtrl);

var _controllersAdminUsersCtrl = require('../controllers/adminUsersCtrl');

var _controllersAdminUsersCtrl2 = _interopRequireDefault(_controllersAdminUsersCtrl);

var _controllersAdminUserCtrl = require('../controllers/adminUserCtrl');

var _controllersAdminUserCtrl2 = _interopRequireDefault(_controllersAdminUserCtrl);

var _controllersProfileCtrl = require('../controllers/profileCtrl');

var _controllersProfileCtrl2 = _interopRequireDefault(_controllersProfileCtrl);

var _module = angular.module('mean-chat.controllers', []).controller('applicationCtrl', _controllersApplicationCtrl2['default']).controller('chatCtrl', _controllersChatCtrl2['default']).controller('loginCtrl', _controllersLoginCtrl2['default']).controller('registerCtrl', _controllersRegisterCtrl2['default']).controller('adminUsersCtrl', _controllersAdminUsersCtrl2['default']).controller('adminUserCtrl', _controllersAdminUserCtrl2['default']).controller('profileCtrl', _controllersProfileCtrl2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../controllers/adminUserCtrl":4,"../controllers/adminUsersCtrl":5,"../controllers/applicationCtrl":6,"../controllers/chatCtrl":7,"../controllers/loginCtrl":8,"../controllers/profileCtrl":9,"../controllers/registerCtrl":10}],17:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _directivesUserList = require('../directives/userList');

var _directivesUserList2 = _interopRequireDefault(_directivesUserList);

var _directivesRoomList = require('../directives/roomList');

var _directivesRoomList2 = _interopRequireDefault(_directivesRoomList);

var _directivesChatPane = require('../directives/chatPane');

var _directivesChatPane2 = _interopRequireDefault(_directivesChatPane);

var _directivesFeedbackMessage = require('../directives/feedbackMessage');

var _directivesFeedbackMessage2 = _interopRequireDefault(_directivesFeedbackMessage);

var _module = angular.module('mean-chat.directives', []).directive('userList', _directivesUserList2['default']).directive('roomList', _directivesRoomList2['default']).directive('chatPane', _directivesChatPane2['default']).directive('feedbackMessage', _directivesFeedbackMessage2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../directives/chatPane":11,"../directives/feedbackMessage":12,"../directives/roomList":13,"../directives/userList":14}],18:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _routesMain = require('../routes/main');

var _routesMain2 = _interopRequireDefault(_routesMain);

var _routesAdmin = require('../routes/admin');

var _routesAdmin2 = _interopRequireDefault(_routesAdmin);

var _module = angular.module('mean-chat.routes', []).config(_routesMain2['default']).config(_routesAdmin2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../routes/admin":20,"../routes/main":21}],19:[function(require,module,exports){
'use strict';

exports.__esModule = true;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _servicesAccountService = require('../services/AccountService');

var _servicesAccountService2 = _interopRequireDefault(_servicesAccountService);

var _servicesAuthService = require('../services/AuthService');

var _servicesAuthService2 = _interopRequireDefault(_servicesAuthService);

var _servicesCurrentUserService = require('../services/CurrentUserService');

var _servicesCurrentUserService2 = _interopRequireDefault(_servicesCurrentUserService);

var _servicesSocket = require('../services/Socket');

var _servicesSocket2 = _interopRequireDefault(_servicesSocket);

var _servicesTokenInterceptor = require('../services/TokenInterceptor');

var _servicesTokenInterceptor2 = _interopRequireDefault(_servicesTokenInterceptor);

var _servicesChat = require('../services/Chat');

var _servicesChat2 = _interopRequireDefault(_servicesChat);

var _servicesAdmin = require('../services/Admin');

var _servicesAdmin2 = _interopRequireDefault(_servicesAdmin);

var _servicesProfileService = require('../services/ProfileService');

var _servicesProfileService2 = _interopRequireDefault(_servicesProfileService);

var _servicesFeedback = require('../services/Feedback');

var _servicesFeedback2 = _interopRequireDefault(_servicesFeedback);

var _module = angular.module('mean-chat.services', []).factory('AccountService', _servicesAccountService2['default']).factory('AuthService', _servicesAuthService2['default']).factory('CurrentUserService', _servicesCurrentUserService2['default']).factory('Socket', _servicesSocket2['default']).factory('TokenInterceptor', _servicesTokenInterceptor2['default']).factory('Chat', _servicesChat2['default']).factory('Admin', _servicesAdmin2['default']).factory('ProfileService', _servicesProfileService2['default']).factory('FeedbackService', _servicesFeedback2['default']);

exports['default'] = _module;
module.exports = exports['default'];

},{"../services/AccountService":23,"../services/Admin":24,"../services/AuthService":25,"../services/Chat":26,"../services/CurrentUserService":27,"../services/Feedback":28,"../services/ProfileService":29,"../services/Socket":30,"../services/TokenInterceptor":31}],20:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	'ngInject';

	var _routeResolves = require('./route-resolves');

	$routeProvider.when('/admin/users', {
		templateUrl: 'src/js/views/admin-users.html',
		controller: 'adminUsersCtrl',
		controllerAs: 'adminUsers',
		access: { requiredLogin: true, isAdmin: true },
		resolve: {
			currentUser: _routeResolves.currentUser,
			users: _routeResolves.users
		}
	}).when('/admin/users/:id', {
		templateUrl: 'src/js/views/admin-user.html',
		controller: 'adminUserCtrl',
		controllerAs: 'adminUser',
		access: { requiredLogin: true, isAdmin: true },
		resolve: {
			currentUser: _routeResolves.currentUser,
			user: _routeResolves.user
		}
	});
}];

module.exports = exports['default'];

},{"./route-resolves":22}],21:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	'ngInject';

	var _routeResolves = require('./route-resolves');

	$routeProvider.when('/', {
		templateUrl: '/src/js/views/chat.html',
		controller: 'chatCtrl',
		controllerAs: 'chat',
		resolve: {
			currentUser: _routeResolves.currentUser
		}
	}).when('/profile', {
		templateUrl: '/src/js/views/profile.html',
		controller: 'profileCtrl',
		controllerAs: 'profile',
		access: { requiredLogin: true },
		resolve: {
			currentUser: _routeResolves.currentUser,
			profile: _routeResolves.profile
		}
	}).when('/login', {
		templateUrl: '/src/js/views/login.html',
		controller: 'loginCtrl',
		controllerAs: 'login',
		access: { requiredLogin: false }
	}).when('/register', {
		templateUrl: '/src/js/views/register.html',
		controller: 'registerCtrl',
		controllerAs: 'register',
		access: { requiredLogin: false }
	});
}];

module.exports = exports['default'];

},{"./route-resolves":22}],22:[function(require,module,exports){
/*----------  Resolve Current User  ----------*/

'use strict';

exports.__esModule = true;
exports.currentUser = currentUser;
exports.profile = profile;
exports.users = users;
exports.user = user;

function currentUser($q, $location, AuthService, CurrentUserService) {
	if (AuthService.isLoggedIn()) {
		return CurrentUserService.getUser();
	} else if (AuthService.userIsReauthenticating) {
		return AuthService.reAuthenticate().then(function () {
			return CurrentUserService.getUser();
		})['catch'](function (res) {
			return $q.reject(res);
		});
	} else {
		$location.path('/login');
		return $q.reject();
	}
}

/*----------  Resolve Profile  ----------*/

function profile($q, ProfileService) {
	return ProfileService.retrieveProfile().then(function (res) {
		return res.data.profile;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  Resolve Admin Users  ----------*/

function users(Admin) {
	return Admin.retrieveUsers().then(function (res) {
		return res.data.users;
	}, function (res) {
		return $q.reject(res);
	});
}

/*----------  Resolve Admin User  ----------*/

function user($q, $route, Admin) {
	return Admin.retrieveUser($route.current.params.id).then(function (res) {
		return res.data.user;
	}, function (res) {
		return $q.reject(res);
	});
}

},{}],23:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var accountService = {};

	accountService.register = function (username, email, password) {
		return $http.post('/api/register', { username: username, email: email, password: password });
	};

	return accountService;
}];

module.exports = exports['default'];

},{}],24:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var Admin = {};

	Admin.retrieveUsers = function () {
		return $http.get('/api/admin/users');
	};
	Admin.deleteUser = function (userID) {
		return $http['delete']('/api/admin/users/' + userID);
	};
	Admin.retrieveUser = function (userID) {
		return $http.get('/api/admin/users/' + userID);
	};
	Admin.updateUser = function (userID, data) {
		return $http.put('/api/admin/users/' + userID, data);
	};
	Admin.updatePassword = function (userID, newPassword) {
		return $http.put('/api/admin/users/' + userID + '/password', { newPassword: newPassword });
	};

	return Admin;
}];

module.exports = exports['default'];

},{}],25:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', '$http', 'CurrentUserService', 'Chat', '$q', function ($rootScope, $http, CurrentUserService, Chat, $q) {
	'ngInject';

	var authService = {};

	authService.userIsReauthenticating = false;

	authService.isLoggedIn = function () {
		return CurrentUserService.user !== null;
	};

	authService.isAdmin = function () {
		if (!CurrentUserService.user) {
			return false;
		}

		return CurrentUserService.user.isAdmin === true;
	};

	authService.logIn = function (username, password) {
		var deferred = $q.defer();

		$http.post('/api/login', { username: username, password: password }).then(function (res) {
			CurrentUserService.setUser(res.data.user, res.data.token);

			$rootScope.$broadcast('auth-login-success');

			deferred.resolve();
		}, function () {
			$rootScope.$broadcast('auth-login-failed');

			deferred.reject();
		});

		return deferred.promise;
	};

	authService.logOut = function () {
		authService.userIsReauthenticating = false;
		Chat.isConnected = false;

		CurrentUserService.destroy();

		$rootScope.$broadcast('auth-logout-success');
	};

	authService.reAuthenticate = function () {
		return $http.get('/api/authenticate').then(function (res) {
			CurrentUserService.setUser(res.data.user);

			authService.userIsReauthenticating = false;

			$rootScope.$broadcast('auth-login-reauthenticate-success');

			return res.data.user;
		}, function () {
			$rootScope.$broadcast('auth-login-reauthenticate-failed');

			return null;
		});
	};

	return authService;
}];

module.exports = exports['default'];

},{}],26:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['Socket', 'CurrentUserService', function (Socket, CurrentUserService) {
	'ngInject';

	var Chat = {};

	Chat.isConnected = false;

	Chat.data = {
		activeUsers: [],
		messages: [],
		rooms: {},
		roomCounts: {}
	};

	Chat.connect = function (currentUser, callback) {
		if (!Chat.isConnected) {
			Socket.emit('join', currentUser, function () {
				Chat.isConnected = true;

				if (angular.isFunction(callback)) {
					callback();
				}
			});
		} else // If chat is already connected and they have returned to this room, i.e. admin/profile pages just reassign the chat data
			{
				if (angular.isFunction(callback)) {
					callback();
				}
			}
	};

	Socket.on('recieveUsers', function (users) {
		Chat.data.activeUsers = users;
	});

	Socket.on('recieveRoomCounts', function (roomCounts) {
		Chat.data.roomCounts = roomCounts;
	});

	Socket.on('message', function (messageObj) {
		Chat.data.messages.push(messageObj);
	});

	Socket.on('recieveRooms', function (rooms) {
		Chat.data.rooms = rooms;
	});

	Socket.on('recieveCurrentRoom', function (currentRoom) {
		Chat.data.currentRoom = currentRoom;
	});

	Socket.on('roomDeleted', function (roomToJoin) {
		Socket.emit('switchRoom', roomToJoin, true);
	});

	Socket.on('recievePrivateChat', function (user) {
		var acceptPrivateChat = confirm(user.username + ' has requested a private chat, would you like to accept?');

		Socket.emit('sendPrivateChatResponse', acceptPrivateChat, user);
	});

	Socket.on('initPrivateChat', function (roomToJoin, rooms) {
		Socket.emit('switchRoom', roomToJoin, false);
		Chat.data.rooms = rooms;
	});

	return Chat;
}];

module.exports = exports['default'];

},{}],27:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$window', function ($window) {
	'ngInject';

	var CurrentUserService = {};

	CurrentUserService.user = null;

	CurrentUserService.setUser = function (user, token) {
		if (user === undefined) {
			throw new Error('setUser requires a valid user object');
			return false;
		}

		CurrentUserService.user = {};

		CurrentUserService.user.userid = user.userid;
		CurrentUserService.user.username = user.username;
		CurrentUserService.user.isAdmin = user.isAdmin;

		if (token !== undefined) {
			$window.sessionStorage.token = token;
		}
	};

	CurrentUserService.getUser = function () {
		return CurrentUserService.user !== null ? CurrentUserService.user : false;
	};

	CurrentUserService.destroy = function () {
		CurrentUserService.user = null;

		delete $window.sessionStorage.token;
	};

	return CurrentUserService;
}];

module.exports = exports['default'];

},{}],28:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$rootScope', function ($rootScope) {
	'ngInject';

	var feedbackService = {};

	$rootScope.$on('$routeChangeSuccess', function () {
		feedbackService.resetMessage();
	});

	feedbackService.message = { text: null, state: null };

	feedbackService.resetMessage = function () {
		feedbackService.message.state = null;
		feedbackService.message.text = null;
	};

	feedbackService.showMessage = function (state, text) {
		if (state === undefined) {
			throw new Error('Please define a state for a feedback message');
		}

		if (text === undefined) {
			throw new Error('Please enter the message for a feedback alert');
		}

		feedbackService.message.state = state;
		feedbackService.message.text = text;
	};

	return feedbackService;
}];

module.exports = exports['default'];

},{}],29:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$http', function ($http) {
	'ngInject';

	var Profile = {};

	Profile.retrieveProfile = function () {
		return $http.get('/api/profile/');
	};
	Profile.updateProfile = function (data) {
		return $http.put('/api/profile', data);
	};

	return Profile;
}];

module.exports = exports['default'];

},{}],30:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['socketFactory', function (socketFactory) {
	'ngInject';

	return socketFactory();
}];

module.exports = exports['default'];

},{}],31:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = ['$q', '$rootScope', '$window', '$location', '$timeout', '$injector', 'CurrentUserService', function ($q, $rootScope, $window, $location, $timeout, $injector, CurrentUserService) {
	'ngInject';

	var tokenInterceptor = {};

	var $http = undefined;

	$timeout(function () {
		$http = $injector.get('$http');
	});

	tokenInterceptor.request = function (config) {
		config.headers = config.headers || {};

		if ($window.sessionStorage.token) {
			config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
		}

		return config;
	};

	tokenInterceptor.responseError = function (rejection) {
		if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || CurrentUserService.user)) {
			$location.path('/login');
			$rootScope.$broadcast('auth-not-authenticated');
		}

		if (rejection != null && rejection.status === 403 && ($window.sessionStorage.token || CurrentUserService.user)) {
			$location.path('/');
			$rootScope.$broadcast('auth-not-authorized');
		}

		if (rejection != null && rejection.status === 500) {
			console.log('tokenInterceptor.js - 500 error handling');
		}

		return $q.reject(rejection);
	};

	return tokenInterceptor;
}];

module.exports = exports['default'];

},{}]},{},[1]);

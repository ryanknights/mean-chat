"use strict";

const express         = require('express'),
	  httpModule      = require('http');

const app             = express(),
	  http            = httpModule.Server(app),
	  bodyParser 	  = require('body-parser'),
	  methodOverride  = require('method-override'),
	  mongoose   	  = require('mongoose'),
	  expressJwt      = require('express-jwt'),
	  jwtSecret       = require('./app/config/secret');

const io = require('socket.io')(http);

const db = require('./app/config/db');

const port = process.env.PORT || 4003;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(methodOverride('X-HTTP-Method-Override'));

app.use(express.static(__dirname + '/public'));

/**
* Chat Socket.IO Events
**/

require('./app/routes/socket')(io);

/**
* Authentication | Protected
**/

app.use('/api/authenticate', expressJwt({secret : jwtSecret.secret}));
app.use('/api/authenticate', require('./app/routes/authenticate'));

/**
* Login/Register | Not Protected
**/

app.use('/api/login', require('./app/routes/login'));
app.use('/api/register', require('./app/routes/register'));

/**
* Profile | Protected
**/

app.use('/api/profile', expressJwt({secret : jwtSecret.secret}));
app.use('/api/profile', require('./app/routes/profile'));

/**
* Admin | Protected
**/
app.use('/api/admin', expressJwt({secret : jwtSecret.secret}));
app.use('/api/admin', require('./app/routes/admin'));

/**
* Default Routes
**/

app.use('/', require('./app/routes/default'));

/**
* Error Handling
**/

app.use((err, req, res, next) => 
{	
	console.log(err.message);

	if (err.message === 'invalid token' || err.message === 'jwt malformed' || err.message === 'jwt expired')
	{
		return res.send(401, 'Invalid Token');	
	}
});

/**
* Start Server
**/

http.listen(port, () =>
{
	console.log('App listening on port ' + port);
});
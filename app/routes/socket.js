"use strict";

const socketCtrl = require('../controllers/socket');

module.exports = (io) =>
{
	io.on('connection', (socket) =>
	{	
		socket.on('join', (user, callback) =>
		{
			socketCtrl.join(io, socket, user, callback);
		});

		socket.on('sendMessage', (message) =>
		{
			socketCtrl.sendMessage(io, socket, message);
		});

		socket.on('switchRoom', (newRoom, afterDeletion, callback) =>
		{
			socketCtrl.switchRoom(io, socket, newRoom, afterDeletion, callback);
		});

		socket.on('createRoom', (newRoomName, maxRoom, callback) =>
		{
			socketCtrl.createRoom(io, socket, newRoomName, maxRoom, callback);
		});

		socket.on('deleteRoom', (roomToDelete, token, callback) =>
		{
			socketCtrl.deleteRoom(io, socket, roomToDelete, token, callback);
		});

		socket.on('disconnect', () =>
		{
			socketCtrl.leaveChat(io, socket);
		});

		socket.on('leaveChat', () =>
		{
			socketCtrl.leaveChat(io, socket);
		});

		socket.on('requestPrivateChat', (userID, callback) =>
		{
			socketCtrl.requestPrivateChat(io, socket, userID, callback);
		});

		socket.on('sendPrivateChatResponse', (acceptChat, user) =>
		{
			socketCtrl.sendPrivateChatResponse(io, socket, acceptChat, user);
		});
	});
};
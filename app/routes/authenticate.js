"use strict"

const express          = require('express'),
	  authenticateCtrl = require('../controllers/authenticate');
	
module.exports = (() =>
{
	var api = express.Router();

	api.get('/', authenticateCtrl.authenticate);

	return api;

})();
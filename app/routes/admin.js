"use strict"

const express   = require('express'),
	  adminCtrl = require('../controllers/admin');
	
module.exports = (() =>
{
	var api = express.Router();

	/*----------  Checks authenticated user is an admin on every request  ----------*/
	
	api.use(adminCtrl.checkIsAdmin);

	/*----------  Retrieve Users  ----------*/
	
	api.get('/users', adminCtrl.retrieveUsers);

	/*----------  Retrieve User  ----------*/

	api.get('/users/:id', adminCtrl.retrieveUser);
	
	/*----------  Delete User  ----------*/

	api.delete('/users/:id', adminCtrl.deleteUser);

	/*----------  Edit User  ----------*/

	api.put('/users/:id', adminCtrl.editUser);

	/*----------  Update Password  ----------*/

	api.put('/users/:id/password', adminCtrl.updatePassword);
	
	return api;

})();
"use strict"

const express     = require('express'),
	  profileCtrl = require('../controllers/profile');

module.exports = (() =>
{
	var api = express.Router();

	api.get('/', profileCtrl.retrieveProfile);
	api.put('/', profileCtrl.updateProfile);

	return api;

})();
"use strict";

const User      = require('../models/User'),
	  jwt       = require('jsonwebtoken'),
	  jwtSecret = require('../config/secret');

exports.login = (req, res, next) =>
{
	let username = req.body.username || '',
		password = req.body.password || '';

	if (username === '' || password === '')
	{
		return res.send(400, 'Please enter a username and password');
	}

	User.findOne({username : username}, (err, user) =>
	{
		if (err)
		{
			return res.send(500, 'There was a problem finding the user');
		}

		if (!user)
		{
			return res.send(401);
		}

		user.comparePassword(password, (isMatch) =>
		{
			if (!isMatch)
			{
				return res.send(401);
			}

			var token = jwt.sign({userid: user._id, isAdmin: user.isAdmin}, jwtSecret.secret, {expiresInMinutes : 60});

			return res.json({user : {username : user.username, userid : user._id, isAdmin : user.isAdmin}, token : token});
		});
	});
}
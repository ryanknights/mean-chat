"use strict";

const User      = require('../models/User'),
	  userUtils = require('./user-utils'),
	  _         = require('lodash');

exports.retrieveProfile = (req, res, next) =>
{
	User.findOne({_id: req.user.userid}, {password: 0}, (err, user) =>
	{
		if (err)
		{
			return res.send(500, 'There was a problem finding the user');
		}

		if (!user)
		{
			return res.send(404, 'No user found');
		}

		let trimmedUser = _.pick(user, ['username', 'email']);

		return res.json({profile: trimmedUser});
	});
}

exports.updateProfile = (req, res, next) =>
{	
	let promises = [];

	if (!req.body.email || !req.body.username)
	{
		return res.send(401, 'Please enter an email address and username');
	}

	User.findOne({_id: req.user.userid}, (err, user) =>
	{
		if (err)
		{
			return res.send(500, 'There was a problem finding the user');
		}

		if (!user)
		{
			return res.send(404, 'No user found');
		}

		if (req.body.username !== user.username)
		{
			promises.push(userUtils.usernameIsUnique(req.body.username));
		}

		if (req.body.email !== user.email)
		{
			promises.push(userUtils.emailIsUnique(req.body.email));
		}

		Promise.all(promises).then((results) =>
		{
			User.update({_id : req.user.userid}, {$set: {username: req.body.username, email: req.body.email}}, (err, user) =>
			{
				if (err || !user)
				{
					return res.send(500, 'There was a problem updating the user');
				}

				return res.json({success: true});
			});

		}, (error) =>
		{
			return res.send(400, error);
		});
	});
}
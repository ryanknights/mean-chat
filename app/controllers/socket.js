"use strict";

const rooms  		   = {Lobby : {creatorID : 0, roomName : 'Lobby', maxUsers : 0, canDelete : false, users : [], isPrivate: false}},
	  defaultRoom      = 'Lobby';

let privateRoomCount = 0;

const utils = require('../controllers/socket-utils')(rooms, defaultRoom, privateRoomCount);

/*=================================================================
=            Sends Users & Entry messages on user join            =
=================================================================*/

exports.join = (io, socket, user, callback) =>
{
	console.log('Socket on join');

	/*----------  Store user on socket  ----------*/
	
	socket.currentUser = user;

	/*----------  Store & join default room  ----------*/
	
	socket.currentRoom = defaultRoom;
	socket.join(defaultRoom);

	/*----------  Add Socket data to relevant room in global array  ----------*/

	utils.addUserToGlobalRooms(socket, socket.currentRoom);

	/*----------  Send updated users list to all connected sockets of the current room  ----------*/
	
	io.sockets.emit('recieveRoomCounts', utils.returnRoomCounts());
	io.sockets.in(socket.currentRoom).emit('recieveUsers', rooms[socket.currentRoom].users);

	/*----------  Send rooms list & current room to this socket (client) ----------*/
	
	socket.emit('recieveRooms', rooms);
	socket.emit('recieveCurrentRoom', {roomName : socket.currentRoom, isPrivate : rooms[socket.currentRoom].isPrivate});
	
	/*----------  Send a message to this socket (client) telling them they are connected  ----------*/
	
	socket.emit('message',
	{
		id      : null,
		name    : 'system',
		message : 'You are now connected to ' + socket.currentRoom,
		time    : Date.now()
	});

	/*----------  Send a message to all clients in the default room a new user has joined  ----------*/

	socket.broadcast.to(socket.currentRoom).emit('message',
	{
		id      : null,
		name    : 'system',
		message : socket.currentUser.username + ' has joined this room',
		time    : Date.now()
	});

	callback();
}	

/*==============================================================
=            Sends message to all connected sockets            =
==============================================================*/

exports.sendMessage = (io, socket, message) =>
{
	console.log('Socket on message');

	io.sockets.in(socket.currentRoom).emit('message',
	{
		id      : socket.currentUser.userid,
		name    : socket.currentUser.username,
		message : message,
		time    : Date.now()
	});	
}

/*====================================
=            Switch Rooms            =
====================================*/

exports.switchRoom = (io, socket, newRoom, afterDeletion, callback) =>
{	
	console.log('Socket on switchRoom');

	/*----------  Validate we can switch room  ----------*/
	
	var canSwitchRoom = utils.validateSwitchRoom(newRoom, socket);

	if (!canSwitchRoom.success)
	{	
		if (callback !== null && typeof callback === 'function')
		{
			return callback(canSwitchRoom.message);
		}
		else
		{
			return false;
		}
	}

	/*----------  Leave current room  ----------*/

	var oldRoom = socket.currentRoom;
	
	socket.leave(socket.currentRoom);

	/*----------  Join new room  ----------*/
	
	socket.join(newRoom);

	/*----------  Send a message to this socket (client) confirming they have switched rooms  ----------*/

	socket.emit('message',
	{
		id      : null,
		name    : 'system',
		message : 'You are now connected to ' + newRoom,
		time    : Date.now()
	});

	/*----------  Send a message to all users in the old room  ----------*/

	socket.broadcast.to(socket.currentRoom).emit('message',
	{
		id      : null,
		name    : 'system',
		message : socket.currentUser.username + ' has left this room',
		time    : Date.now()
	});

	/*----------  Send a message to all users in the new room exclusing this socket (client)  ----------*/

	socket.broadcast.to(newRoom).emit('message',
	{
		id      : null,
		name    : 'system',
		message : socket.currentUser.username + ' has joined this room',
		time    : Date.now()
	});

	/*----------  Update stored room on the socket  ----------*/

	socket.currentRoom = newRoom;

	/*----------  Send new current room to this socket (client)  ----------*/

	socket.emit('recieveCurrentRoom', {roomName : socket.currentRoom, isPrivate : rooms[socket.currentRoom].isPrivate});

	/*----------  Remove user from current room in global array and add back into new room then send updated list to both old room & new room sockets  ----------*/

	utils.removeUserFromGlobalRooms(socket.currentUser.userid);
	utils.addUserToGlobalRooms(socket, socket.currentRoom);

	io.sockets.emit('recieveRoomCounts', utils.returnRoomCounts());
	io.sockets.in(socket.currentRoom).emit('recieveUsers', rooms[socket.currentRoom].users);

	if (afterDeletion === false)
	{
		io.sockets.in(oldRoom).emit('recieveUsers', rooms[oldRoom].users);
	}
}

/*==============================================================================
=            Creates new room and emits room list back to all users            =
==============================================================================*/

exports.createRoom = (io, socket, newRoomName, maxUsers, callback) =>
{	
	console.log('Socket on createRoom');

	/*----------  Validate we can add the room  ----------*/
	
	var isValidRoomName = utils.validateNewRoom(newRoomName);

	if (!isValidRoomName.success)
	{
		if (callback !== null && typeof callback === 'function')
		{
			return callback(isValidRoomName.message);
		}
		else
		{
			return false;
		}		
	}

	/*----------  Push new room into global array  ----------*/
	
	rooms[newRoomName] = 
	{
		creatorID : socket.currentUser.userid,
		roomName  : newRoomName,
		maxUsers  : maxUsers || 0,
		canDelete : true,
		users     : [],
		isPrivate : false
	};

	/*----------  Emit rooms to all connected sockets  ----------*/
	
	io.sockets.emit('recieveRooms', rooms);

	callback(null);
}

/*==========================================================================
=            Deletes room and moves existing users to the lobby            =
==========================================================================*/

exports.deleteRoom = (io, socket, roomToDelete, token, callback) =>
{	
	console.log('Socket on deleteRoom');
	
	/*----------  Validate the user is allowed to delete the room  ----------*/

	var isAllowedToDelete = utils.validateDeleteRoom(socket, roomToDelete, token);

	if (!isAllowedToDelete.success)
	{
		if (callback !== null && typeof callback === 'function')
		{
			return callback(isAllowedToDelete.message);
		}
		else
		{
			return false;
		}		
	}
	
	/*----------  Emit a message to all sockets connected to the deleted room  ----------*/
	
	io.sockets.in(roomToDelete).emit('message',
	{
		id      : null,
		name    : 'system',
		message : roomToDelete + ' has been deleted, you are returning to ' + defaultRoom,
		time    : Date.now()		
	});

	/*----------  Emit a roomDeleted message to sockets connected which in turn basically calls switchRoom  ----------*/
	
	io.sockets.in(roomToDelete).emit('roomDeleted', defaultRoom);

	/*----------  Delete room from global array  ----------*/

	delete rooms[roomToDelete];

	/*----------  Send back list of udpdated rooms  ----------*/
	
	io.sockets.emit('recieveRooms', rooms);
}

/*=======================================================================================
=            Sends users & leave message when a user logs out or disconnects            =
=======================================================================================*/

exports.leaveChat = (io, socket) =>
{	
	console.log('Socket on leave');

	if (socket.currentUser === undefined)
	{	
		return console.log('No currentUser is defined');
	}

	var username = socket.currentUser.username;

	utils.removeUserFromGlobalRooms(socket.currentUser.userid);

	io.sockets.emit('recieveRoomCounts', utils.returnRoomCounts());
	socket.broadcast.to(socket.currentRoom).emit('recieveUsers', rooms[socket.currentRoom].users);

	socket.broadcast.to(socket.currentRoom).emit('message',
	{
		id      : null,
		name    : 'system',
		message : username + ' has disconnected',
		time    : Date.now()
	});

	/*----------  If all users have disconnected and have access to a private room, delete it  ----------*/
	
	if (rooms[socket.currentRoom].isPrivate && !rooms[socket.currentRoom].users.length)
	{	
		console.log('Auto delete private chat room');

		delete rooms[socket.currentRoom];
	}
}

/*==================================================================================
=            Request recieved to start a private chat with another user            =
==================================================================================*/

exports.requestPrivateChat = (io, socket, userID, callback) =>
{
	console.log('Socket on requestPrivateChat');

	var userRequested = utils.findUser(userID);

	if (!userRequested)
	{
		if (callback !== null && typeof callback === 'function')
		{
			return callback('This user does not exist');
		}
		else
		{
			return false;
		}		
	}

	io.sockets.to(userRequested.socketid).emit('recievePrivateChat', socket.currentUser);

	socket.emit('message',
	{
		id      : null,
		name    : 'system',
		message : 'A private chat request has been sent to ' + userRequested.username + ' you will be notified of their response.',
		time    : Date.now()
	});
}

/*===========================================================================================================================
=            Response recieved for a private chat request, this needs to handle feedback for both users involved            =
===========================================================================================================================*/

exports.sendPrivateChatResponse = (io, socket, chatAccepted, user) =>
{
	console.log('Socket on sendPrivateChatResponse');

	let initialUserRequested = utils.findUser(user.userid);

	/*----------  Chat has not been accepted, send back response to the initial user who requested it  ----------*/
	
	if (!chatAccepted)
	{
		io.sockets.to(initialUserRequested.socketid).emit('message',
		{
			id      : null,
			name    : 'system',
			message : 'The private chat you sent to ' + socket.currentUser.username + ' has been declined.',
			time    : Date.now()			
		});
	}
	else // Chat has been accepted so we need to create private room, notify both users and switch them to the new room
	{
		io.sockets.to(initialUserRequested.socketid).emit('message',
		{
			id      : null,
			name    : 'system',
			message : 'The private chat you sent to ' + socket.currentUser.username + ' has been accepted you will now be directed to the private room.',
			time    : Date.now()			
		});

		socket.emit('message',
		{
			id      : null,
			name    : 'system',
			message : 'You have accepted the private chat you recieved from ' + initialUserRequested.username + ' you will now be directed to the private room.',
			time    : Date.now()				
		});

		/*----------  Push new room into global array  ----------*/

		let privateRoomName = 'Private Room ' + privateRoomCount++;

		rooms[privateRoomName] = 
		{
			creatorID        : initialUserRequested.userid,
			roomName         : privateRoomName,
			maxUsers         : 2,
			canDelete        : true,
			users            : [],
			isPrivate        : true,
			allowedAccessIDs : [initialUserRequested.userid, socket.currentUser.userid]
		};

		io.sockets.to(initialUserRequested.socketid).emit('initPrivateChat', privateRoomName, rooms);
		socket.emit('initPrivateChat', privateRoomName, rooms);	
	}
}
"use strict";

const User = require('../models/User');

exports.authenticate = (req, res, next) =>
{
	if (!req.user)
	{
		return res.send(401);
	}

	User.findOne({_id: req.user.userid}, (err, user) =>
	{
		if (err)
		{
			return res.send(500, 'There was a problem finding the user');
		}

		if (!user)
		{
			return res.send(401, 'No user could be found');
		}

		return res.json({user : {username : user.username, userid : user._id, isAdmin : user.isAdmin}});
	});	
}
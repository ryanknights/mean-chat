"use strict";

module.exports = (rooms, defaultRoom, privateRoomCount) =>
{
	const jwt       = require('jsonwebtoken'),
		  jwtSecret = require('../config/secret');

	return {

		removeUserFromGlobalRooms (removeID)
		{	
			for (var room in rooms)
			{	
				var roomUsers = rooms[room].users;

				for (var i = roomUsers.length; i--;)
				{
					if (roomUsers[i].userid === removeID)
					{	
						roomUsers.splice(i, 1);

						return true;
					}	
				}	
			}

			return false;
		},

		/*==========================================
		=            Validates new room            =
		==========================================*/

		validateNewRoom (newRoom)
		{	
			if (newRoom === '')
			{
				return {success : false, message : 'Please enter a room name'};
			}

			for (var roomName in rooms)
			{
				if (newRoom === roomName)
				{
					return {success : false, message : 'This room name already exists'};
				}		
			}

			return {success : true };
		},

		/*=========================================================================================
		=            Adds a user to the global activeUsers array in the specified room            =
		=========================================================================================*/

		addUserToGlobalRooms (socket, room)
		{
			rooms[room].users.push(
			{
				userid     : socket.currentUser.userid,
				username   : socket.currentUser.username,
				socketid   : socket.id
			});	
		},

		/*==============================================================
		=            Finds a user based on the given userID            =
		==============================================================*/

		findUser (userID)
		{
			for (var room in rooms)
			{
				var roomUsers = rooms[room].users;

				for (var i = roomUsers.length; i--;)
				{
					if (roomUsers[i].userid === userID)
					{
						return roomUsers[i];
					}
				}
			}

			return false;
		},

		/*=================================================
		=            Validates a room deletion            =
		=================================================*/

		validateDeleteRoom (socket, roomToDelete, token)
		{	
			try
			{
				var tokenData = jwt.verify(token, jwtSecret.secret);
			}
			catch (err)
			{
				return {success : false, message : 'Your request is invalid'}
			}
			
			if (socket.currentUser.userid !== tokenData.userid)
			{
				return {success : false, message : 'Your request is invalid'}
			}

			if (socket.currentUser.userid !== rooms[roomToDelete].creatorID && !socket.currentUser.isAdmin)
			{
				return {success : false, message : 'You do not have access to delete this room'}
			}	

			return {success : true};
		},

		/*============================================
		=            Validate switch room            =
		============================================*/

		validateSwitchRoom (newRoomName, socket)
		{	
			var newRoom = rooms[newRoomName];

			if (!newRoom)
			{
				return {success: false, message: 'This room does not exist'}
			}

			if (newRoom.isPrivate && newRoom.allowedAccessIDs.indexOf(socket.currentUser.userid) === -1)
			{
				return {success: false, message : 'You have not been invited to enter this room'}
			}

			if (newRoom.maxUsers !== 0 && newRoom.users.length >= newRoom.maxUsers)
			{
				return {success: false, message : 'This room is currently full'}
			}

			return {success: true}
		},

		/*========================================================================
		=            Returns an object of how many users in each room            =
		========================================================================*/

		returnRoomCounts ()
		{
			var roomsCount = {};

			for (var room in rooms)
			{
				roomsCount[room] = rooms[room].users.length || 0;
			}

			return roomsCount;
		}
	}
}